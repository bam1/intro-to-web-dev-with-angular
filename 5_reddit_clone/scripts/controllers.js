angular.module('feed-reader')

.controller('homeController', ['$scope', function($scope){
}])

.controller('nbaController', ['$scope', 'redditService', function($scope, redditService){
  $scope.title = 'NBA';
  redditService.getData('nba').then(function(data){
    $scope.stories = data;
  });
}])


.controller('nflController', ['$scope', 'redditService', function($scope, redditService){
  $scope.title = 'NFL';
  redditService.getData('nfl').then(function(data){
    $scope.stories = data;
  });
}])


.controller('mlbController', ['$scope', 'redditService', function($scope, redditService){
  $scope.title = 'MLB';
  redditService.getData('mlb').then(function(data){
    $scope.stories = data;
  });
}])